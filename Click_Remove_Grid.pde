// Declare lists.
IntList xVals;
IntList yVals;

// Run once
void setup() {
  // Set canvas size.
  size(501, 501);
  // Initilize Lists
  xVals = new IntList();
  yVals = new IntList();
}

// Repeats
void draw() {
  // Clear background, and draw grid.
  background(51);
  stroke(255);
  for (int x = 0; x < width; x = x + 20) {
    for (int y = 0; y < height; y = y + 20) {
      point(x, y);
    }
  }

  // Fill in pressed tiles.
  for (int x = 0; x < xVals.size(); x = x + 1) {
    rectMode(CORNER);
    rect(xVals.get(x) * 20, yVals.get(x) * 20, 20, 20);
  }

  // Print # of tiles pressed.
  textSize(12);
  text(xVals.size(), 7, 495);
}

// Fires when event is triggered.
void mouseClicked() {
  // Checks each tile
  for (int x = 0; x < width; x = x + 1) {
    for (int y = 0; y < height; y = y + 1) {
      // Checking if mouse clicked position is within tile.
      if (mouseContain(x * 20, y * 20, 20, 20, pmouseX, pmouseY) == true) {
        // Checks of tile had already been pressed.
        if (alreadyThere(x, y) == false) {
          // If tile has not been pressed then add it.
          addTile(x, y);
        } else {
          // If the tile has been pressed then remove it.
          removeTile(x, y);
        }
      }
    }
  }
}

// Function to check if mouse is within in a area.
boolean mouseContain(int areaX, int areaY, int areaWidth, int areaHeight, int mX, int mY) {
  boolean areaContains = false;
  if (mX > (areaX) && mX < (areaX + areaWidth) && mY > (areaY) && mY < (areaY + areaHeight)) {
    areaContains = true;
  }
  return areaContains;
}

// Function to check if given (x, y) values are within an initilized list.
boolean alreadyThere(int x, int y) {
  boolean contained = false;
  for (int i = 0; i < xVals.size(); i = i + 1) {
    if (xVals.get(i) == x && yVals.get(i) == y) {
      contained = true;
    }
  }
  return contained;
}

// Adds given (x, y) to initilized lists.
void addTile(int x, int y) {
  xVals.append(x);
  yVals.append(y);
}

// Finds given (x, y) values in initlized lists and removes given (x, y) values.
void removeTile(int x, int y) {
  for (int i = 0; i < xVals.size(); i = i + 1) {
    if (xVals.get(i) == x && yVals.get(i) == y) {
      xVals.remove(i);
      yVals.remove(i);
    }
  }
}